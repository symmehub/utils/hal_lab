## HAL data extraction for a Laboratory

This repository contains scripts :

- That extract data from [HAL](https://hal.science/) for a given lab and generates a graph of the number of publications per year. The script uses the [HAL API](https://api.archives-ouvertes.fr/docs) to extract the data and the Matplotlib library to generate the graph.
- That  extract the details of the papers from HAL and take a screenshot of the paper page.

The lab identification and other settings are stored in a `config.yml` file. (See below for more details.)

The CI produces a web page using GitLab Pages with the graph (see examples https://symmehub.gitlab.io/utils/hal_lab/).

Each image (graph and screenshot) have a species web address :
-  https://symmehub.gitlab.io/utils/hal_lab/imgArtByYear_ART.png
-  https://symmehub.gitlab.io/utils/hal_lab/imgArtByYear_COMM.png
-  https://symmehub.gitlab.io/utils/hal_lab/0_paper.png
-  https://symmehub.gitlab.io/utils/hal_lab/1_paper.png
-  ... 

The web page and the graph can be used to show the activity of the lab, using digital signage software like [Xiboo](https://xibosignage.com/).

It produces a graph like this:

![Graph](src/imgArtByYear_ART.png)

and take a screenshot of the paper page like this:

![Screenshot](src/snapShot/0_paper.png)


### How install and use it

You have 2 possibilities:

#### Local Installation

Follow these steps to install and run the GenGraph and ArtdetailExtract script locally:

1. Clone the repository to your local machine using `git clone <repository_url>`.
2. Navigate to the cloned repository using `cd <repository_name>`.
3. Install the required Python packages using `pip install -r requirements.txt`.
4. Install playwright using `pip install playwright` and then `playwright install`.
5. Ensure that the `config.yml` file is set up correctly as per the instructions in the Configuration File section.
6. Run the GenGraph script 
7. Or Run the ArtdetailExtract script

Please ensure that you have Python 3.6 or later installed on your machine before following these steps.

#### Use it directly on gitlab with CI capacities

You can also use the GenGraph script directly on GitLab and take advantage of GitLab's Continuous Integration (CI) capabilities. 
1. **Clone the repository to your local machine.** 

2. **Modify and push the `config.yml` file to your GitLab repository.** 

3. The CI in `.gitlab-ci.yml` should start automatically, if not you can trigger it manually.
4. It will update the page `xxx.gitlab.io` with the graph and the screenshot of the papers. 


### Configuration File (`config.yml`)

The `config.yml` file is used to set up various parameters for the GenGraph script. Here's a brief explanation of each parameter:

- `labHalId`: The HAL id of the structure. It's a string value.
- `labNameForDisplay`: The name of the lab for display. It's a string value.
- `min_year`: The first year to consider for the statistics. It's an integer value.
- `legend_dict`: The legend of the different types of publications in the plots. For example, 'ART' is mapped to 'Article', 'COMM' is mapped to 'Communication'.
- `colors_dict`: Colors for the different types of publications in the plots. For example, 'ART' is mapped to '#8d0e2f', 'COMM' is mapped to '#d77e95'.
- `nb_paper_to_extract`: The number of papers to extract for screen capture. It's an integer value.
- `cropt_corner`: The coordinates of the top left and bottom right corners of the area to crop for the screen capture. It's a dictionary with 'left', 'top', 'right', and 'bottom' keys.
- `output_width`: The width of the output image. It's an integer value.

Please ensure that the `config.yml` file is in the root directory.

### License
General Public License v3.0

