# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 21:15:25 2021

@author: rouxemi
"""
import requests
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from datetime import date
import yaml
#set backen to Agg
plt.switch_backend('Agg')

def get_stat(Type='ART', lab='lab_name_in_HAL'):
    url = 'https://api.archives-ouvertes.fr/search/?q=structure_t:{1}&start=0&rows=0&wt=phps&omitHeader=true&facet.mincount=1&facet.limit=10000&facet=true&facet.field=publicationDateY_i&facet.sort=index&fq=docType_s:{0}'.format(Type,lab)
    response = requests.get(url=url, 
                            timeout=50)
    
    data = response.json()
    print(data)
    artByY = data['facet_counts']['facet_fields']['publicationDateY_i']
    
    Y =np.array(artByY[::2]).astype(np.int32)
    ArtCount =np.array(artByY[1::2]).astype(np.int32)
    
    ar = np.array([ArtCount]).T
    df = pd.DataFrame(ar, index = Y, columns = [Type])
    
    return df

def plot_hist(ax=None, Type='ART', lab='lab_name_in_HAL'):
    p1 = ax.bar(df_t.index, df_t[Type],width = 0.8 , label=legend_dict[Type], color=colors_dict[Type])
    ax.legend()
    ax.bar_label(p1, label_type='edge', color='k')
    ax.set(yticklabels=[])  # remove the tick labels
    ax.tick_params(left=False)  # remove the ticks
    ax.set_frame_on(False)
    ax.set_xlabel(lab + ' / Extraction HAL : ' + date, size=6, loc='right', color='gray')
    plt.savefig('./imgArtByYear_{}.png'.format(Type), dpi=400)

# Read Config File
with open('../config.yml', 'r') as config_file:
    config = yaml.safe_load(config_file)

labHalId = config['labHalId']
labNameForDisplay = config['labNameForDisplay']
min_year = config['min_year']
legend_dict = config['legend_dict']
colors_dict = config['colors_dict']

# Get Data
df = get_stat(Type='ART', lab = labHalId)
df = df.join(get_stat(Type='COMM', lab = labHalId))
df['Lab'] = labHalId

# Filter Data
df_t = df[df.index>min_year]

# Plot
today = date.today()
date = today.strftime("%d/%m/%Y")

fig, ax = plt.subplots(figsize=(8,6))
plot_hist(ax=ax, Type='ART', lab = labNameForDisplay)

fig, ax = plt.subplots(figsize=(8,6))
plot_hist(ax=ax, Type='COMM', lab = labNameForDisplay)




