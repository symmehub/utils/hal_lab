import requests
import pandas as pd 

from PIL import Image
from playwright.sync_api import sync_playwright
import yaml

"""
This script is used to extract the details of the papers from HAL and take a screenshot of the paper page.
"""


def tack_screen_shoot(playwright, url, out_file_name="screen_shot"):   
    """
    Take a screenshot of a webpage using Playwright
    """
    adress = url
    adress = adress.replace('univ-eiffel.', '')
    print("\t target page: ",adress)

    # launch the browser
    browser = playwright.chromium.launch()
    print('\t browser launched')
    # opens a new browser page
    page = browser.new_page()
    print('\t page opened')
    # navigate to the website
    page.goto(adress)
    print('\t page loaded')
    # take a full-page screenshot
    screenshot = page.screenshot(path=out_file_name+'.png', full_page=True)
    # always close the browser
    browser.close()

    return screenshot

def crop_img(fname, cropt_corner = {}, output_width = 1300): 
    """
    Crop the image to keep only the main content
    """
    # Opens a image in RGB mode
    im = Image.open(fname)
    
    # Size of the image in pixels (size of original image)
    # (This is not mandatory)
    width, height = im.size
    
    # Setting the points for cropped image : top-left corner, and bottom-right corner
    left = cropt_corner['left'] 
    top = cropt_corner['top']
    right = cropt_corner['right']
    bottom = cropt_corner['bottom']

    
    # Cropped image of above dimension
    # (It will not change original image)
    im1 = im.crop((left, top, right, bottom))
    #resize the image
    a = output_width/im1.size[0] # set width to 1300
    im1 = im1.resize( [int(a * s) for s in im1.size] )
    im1.save(fname)


# Read Config File
with open('../config.yml', 'r') as config_file:
    config = yaml.safe_load(config_file)

labHalId = config['labHalId']
nb_paper_to_extract = config['nb_paper_to_extract']
cropt_corner = config['cropt_corner']
output_width = config['output_width']



url = f"http://api.archives-ouvertes.fr/search/?q=structure_t:{labHalId}&fq=docType_s:ART&fq=submittedDateY_i:[2015 TO *]&wt=json&fl=title_s,label_s,producedDate_s,docid,uri_s,doiId_s,authLastName_s,abstract_s&sort=producedDate_s desc"
response = requests.get(url=url, 
                        timeout=50)

data = response.json()
df = pd.DataFrame.from_dict(data['response']['docs'])
with sync_playwright() as playwright:
    for i in range(nb_paper_to_extract):
        f_name = "./snapShot/"+str(i)+'_'+'paper'
        uri_s = str(df.loc[i,'uri_s'])
        if ~(uri_s=='nan'):
            print("\n - Paper url on hal : ", uri_s)
            screenshot = tack_screen_shoot(playwright, uri_s, out_file_name=f_name)
            crop_img(f_name+'.png', cropt_corner = cropt_corner)
            print("\t "+ f_name + " taken.")









