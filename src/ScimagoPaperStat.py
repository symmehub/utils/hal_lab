import requests
import pandas as pd 
import matplotlib.pyplot as plt
import yaml
# set backend to Agg
plt.switch_backend('Agg')


# Read Config File
with open('../config.yml', 'r') as config_file:
    config = yaml.safe_load(config_file)

report_path="../reports/"

# Get the data from HAL
needed_fields = ['producedDate_s', 'publicationDate_tdate', 'title_s', 'label_s', 'docid', 'uri_s', 'doiId_s', 'journalTitle_s', 'journalIssn_s', 'authLastName_s', 'abstract_s']

# lab idenification
labHalId = config['labHalId']
labNameForDisplay = config['labNameForDisplay']

#make url request to hal with the needed fields
needed_fields_str = ','.join(needed_fields)
url = f"http://api.archives-ouvertes.fr/search/?q=structure_t:{labHalId}&fq=docType_s:ART&wt=json&fq=submittedDateY_i:[2019 TO *]&fl={needed_fields_str}&sort=producedDate_s desc&rows=5000"
print('api request:\n', url)

response = requests.get(url=url, 
                        timeout=100)

data = response.json()
df = pd.DataFrame.from_dict(data['response']['docs'])
# remove '-' in all data of the column journalIssn_s
df['journalIssn_s'] = df['journalIssn_s'].str.replace('-', '')
# convert publicationDate_tdate to datetime and producedDate_s
df['publicationDate_tdate'] = pd.to_datetime(df['publicationDate_tdate'])
df['producedDate_s'] = pd.to_datetime(df['producedDate_s'])

# ### SCIMAGO DATA ####
# Read scimago data
scimago = pd.read_csv('../data/scimagojr 2023.csv', sep=';')
# keep only the columns needed : Title, Issn, SJR Best Quartile, Categories
scimago = scimago[['Title', 'Issn', 'SJR Best Quartile', 'Categories']]
# remove SPACE in the column names
scimago.columns = scimago.columns.str.replace(' ', '')

# Split the Issn column in scimago to handle multiple ISSNs
scimago['Issn'] = scimago['Issn'].str.split(', ')

# Explode the Issn column to have one ISSN per row
scimago = scimago.explode('Issn')

# get the SJR Best Quartile paper in df using the Issn
df = df.merge(scimago, left_on='journalIssn_s', right_on='Issn', how='left')

# sort the data by producedDate_s
df = df.sort_values(by='producedDate_s', ascending=False)
# repalce NaN by '-'
df['SJRBestQuartile'] = df['SJRBestQuartile'].fillna('-')

# groupe paper by year of publication and SJR Best Quartile
df_grouped = df.groupby([df['producedDate_s'].dt.year, 'SJRBestQuartile']).size().unstack()
#replace NaN by 0
df_grouped = df_grouped.fillna(0)
#limite data from 2018 to now
df_grouped = df_grouped.loc[2019:]

# save the data
df.to_csv(report_path+f'{labNameForDisplay}_Publication_liste.csv')
# Remove timezone information from datetime columns
df['publicationDate_tdate'] = df['publicationDate_tdate'].dt.tz_localize(None)
df['producedDate_s'] = df['producedDate_s'].dt.tz_localize(None)
# export data to excel by colloring the line with the SJR Best Quartile
# create a color map for the SJR Best Quartile
colors = {'-': '#d9d9d9', 'Q4': '#dd5a4e', 'Q3': '#fbac64', 'Q2': '#e8d559', 'Q1': '#a4cf63'}
# create a style for the excel file
def color_line(x):
    return ['background-color: ' + colors[x['SJRBestQuartile']] for i in x]
# apply the style to the dataframe
df.style.apply(color_line, axis=1).to_excel(report_path+f'{labNameForDisplay}_Publication_liste.xlsx', engine='openpyxl')



#df.to_excel(report_path+f'{labNameForDisplay}_Publication_liste.xlsx')


# make a hist plot of the number of paper by year and SJR Best Quartile
# set Q1 on top, then Q2, Q3, Q4  and -
df_grouped = df_grouped[['-', 'Q4', 'Q3', 'Q2', 'Q1']]
# set color for each SJR Best Quartile : Q1 in green, Q2 in yellow, Q3 in orange, Q4 in red and - in grey
colors = ['#d9d9d9', '#dd5a4e', '#fbac64', '#e8d559', '#a4cf63']

# function that make a readable color by increasing the saturation
# colors a given in HEXA, like #d9d9d9
# convert to HSV and increase the saturation
# convert back to HEXA
import colorsys
def readable_color(color):
    color = color.lstrip('#') 
    r, g, b = tuple(int(color[i:i+2], 16) for i in (0, 2, 4))
    # set color to (0,1) range
    r = r / 255.
    g = g / 255.
    b = b / 255.
    h, s, v = colorsys.rgb_to_hsv(r, g, b)
    #s = s + .4
    if v < 0.5 : v = 0.6
    else : v = 0.4
    r, g, b = colorsys.hsv_to_rgb(h, s, v)
    # push back to 0-255 range
    r = int(r * 255)
    g = int(g * 255)
    b = int(b * 255)
    r = max(0, min(r, 255))
    g = max(0, min(g, 255))
    b = max(0, min(b, 255))
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)

df_grouped.plot(kind='bar', stacked=True, color=colors)
# add the number of each categorie on top of the bar, in the middle
# no need if the vlaue is zero or 0
for i in range(len(df_grouped)):
    for j in range(len(df_grouped.columns)):
        if df_grouped.iloc[i, j] > 1:
            plt.text(i, df_grouped.iloc[i, :j].sum() + df_grouped.iloc[i, j] / 2, int(df_grouped.iloc[i, j]), ha='center', va='center', color=readable_color(colors[j]))
# write on top of each bar the total of paper of the year
for i in range(len(df_grouped)):
    plt.text(i, df_grouped.iloc[i].sum()+.5, int(df_grouped.iloc[i].sum()), ha='center', va='center', color='black')

# set legend outside of the plot
plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

# set the year tick at 45°
plt.xticks(rotation=45)
plt.xlabel('')

# add title and labels
plt.title(f'{labNameForDisplay} - Number of paper by year and SJR Best Quartile')
# foot note, small text in grey at the bottom right of the plot
# with the texte 'Data from HAL and Scimago 2024 - www.scimagojr.com' and the date of the plot
today = pd.to_datetime('today').strftime('%Y-%m-%d')
plt.figtext(0.99, 0.01, 'Data from HAL and Scimago 2024 - www.scimagojr.com\n' + today, horizontalalignment='right', color='grey', fontsize=6)

# make the plot bigger to includ eth elegend
plt.tight_layout()
# remove y axis and ticks
plt.yticks([])
plt.tick_params(left=False)

# remove the frame but keep the x axis
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['left'].set_visible(False)


#save the plot
plt.savefig( report_path + f'{labNameForDisplay}_ScimagoStat.png', dpi=600)

